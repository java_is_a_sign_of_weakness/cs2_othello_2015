#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <vector>
#include "common.h"
#include "board.h"
#include "minimaxnode.h"
//using namespace std;

// midgame scoring
#define OPPONENT_CORNER 10
#define SELF_CORNER 3
#define OPPONENT_EDGE 1
#define SELF_EDGE 2
#define OPPONENT_NEXTTOCORNER 1
#define SELF_NEXTTOCORNER 2

// phase definition
#define BOOK 1
#define OPENING 2
#define MIDGAME 3
#define LATEGAME 4
#define ENDGAME 5

#define MIDTHRESHOLD 10 // opening -> midgame
#define LATETHRESHOLD 20 // mid -> late game
#define ENDTHRESHOLD 25 // late -> end game
#define MIDMOBILITYWEIGHT 3

class MinimaxNode;
class RecursiveNode;

class Player {
private:
    Board *board;
    Side player_side, opponent_side;
    std::vector<Point> nextToCorners;
    std::vector<Point> edges;

    std::vector<Move *> getMovesBad(Board * board, Side side);
    std::vector<Move *> getMoves(Board * board, Side side);
    Move *getMove(Move *opponentsMove);
    Move *getHeuristicMove(Board * board, Side side);
    Move *getRandomMove();
    int score(Board *board);
    int simple_score(Board *board);
    int better_score(Board *board);
    int real_score(Board *board);
    int mobility(Board *board);
    int special_move_score(Board *board);
    MinimaxNode *startNode;
    RecursiveNode *getMinimax(int levels, Side s, RecursiveNode *lastMove);
    Move *getMinimaxMove2D();
    int phase; // of the game.
    int numTurns;
    
public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    
    // Set the board to a new board
    void setBoard(Board *newBoard);
    
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
