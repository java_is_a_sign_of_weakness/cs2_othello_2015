#ifndef __MINIMAXNODE_H__
#define __MINIMAXNODE_H__

#include <iostream>
#include <vector>
#include "common.h"
#include "board.h"
#include "player.h"
//using namespace std;

// To avoid issues with the circular dependency
class Player;

/**
 *
 */
class MinimaxNode {
	
private:
    Move *move;
    Board *board;
    int minimaxScore;
    bool scored;
    std::vector<MinimaxNode *> children;
    friend class Player;
    friend class RecursiveNode;
public:
    MinimaxNode(Move *move, Board *board);
    virtual ~MinimaxNode();
    // Add a child to the node
    void addChild(MinimaxNode *node) {
	children.push_back(node);
    }
    Move *getMove() {
	return move;
    }
    void setScore(int score) {
	this->minimaxScore = score;
	scored = true;
    }
    int getScore() {
	return this->minimaxScore;
    }
};

/**
 * Using polymorphism to minimize effort for making RecursiveNode
 * Although RecursiveNode can add normal MinimaxNodes as children,
 * it is highly recommended that you add RecursiveNodes as children
 */
class RecursiveNode: public MinimaxNode {
public:
    RecursiveNode(Move *move, Board *board) : MinimaxNode(move, board) {
    }
    bool allScored() {
	for (unsigned int i = 0; i < children.size(); i++)
	{
	    if (!children[i]->scored)
	    {
		return false;
	    }
	}
	return true;
    }
};

#endif //__MINIMAXNODE_H__
