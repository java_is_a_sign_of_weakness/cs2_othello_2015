#include <vector>
// for debugging
#include <cstdio>
#include <stdlib.h>
#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side){
	// Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    phase = OPENING;

    // Keep track of what side player and opponent are on
    if (side == WHITE)
    {
	player_side = WHITE;
	opponent_side = BLACK;
    }
    else
    {
	player_side = BLACK;
	opponent_side = WHITE;
    }
    
    // Make the player's internal board object
    board = new Board();

    // Add the corners to nextToCorners
    nextToCorners.push_back(Point(1, 0));
    nextToCorners.push_back(Point(0, 1));
    nextToCorners.push_back(Point(1, 1));
    nextToCorners.push_back(Point(6, 7));
    nextToCorners.push_back(Point(7, 6));
    nextToCorners.push_back(Point(6, 6));
    nextToCorners.push_back(Point(1, 7));
    nextToCorners.push_back(Point(0, 6));
    nextToCorners.push_back(Point(1, 6));
    nextToCorners.push_back(Point(7, 1));
    nextToCorners.push_back(Point(6, 0));
    nextToCorners.push_back(Point(6, 1));

    // Add the edges to edges
    // IMPORTANT: only push the non-corner edges
    for (int i = 2; i < 6; i++)
    {
	edges.push_back(Point(0, i));
    }
    for (int i = 2; i < 6; i++)
    {
	edges.push_back(Point(i, 0));
    }
    for (int i = 2; i < 6; i++)
    {
	edges.push_back(Point(7, i));
    }
    for (int i = 2; i < 6; i++)
    {
	edges.push_back(Point(i, 7));
    }
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete board;
}

/**
 * @brief get list of available moves
 */
std::vector<Move *> Player::getMoves(Board * board, Side side)
{
    // For now, just use the bad way
    return getMovesBad(board, side);
}

/**
 * @brief naive implementation of a method to get all available moves
 */
std::vector<Move *> Player::getMovesBad(Board * board, Side side)
{
    std::vector<Move *> ans;
    // Naively check every single move and add if valid
    for (int i = 0; i < 8; i++)
    {
	for (int j = 0; j < 8; j++)
	{
	    Move *m = new Move(i, j);
	    if (board->checkMove(m, side))
	    {
		ans.push_back(m);
	    }
	    else
	    {
		// if not going to use it, free the memory
		delete m;
	    }
	}
    }
    return ans;
}

Move *Player::getMove(Move *opponentsMove)
{
    if (testingMinimax){
	return getMinimaxMove2D();
    }
    //return getMinimaxMove2D();
    Board *b = board->copy();
    b->doMove(opponentsMove, opponent_side);
    RecursiveNode *n = new RecursiveNode(opponentsMove, b);
    RecursiveNode *n2;
    n2 = getMinimax(4, opponent_side, n);
    Move *m = n2->getMove();
    // Simple hack to prevent playing an invalid move
    if (board->checkMove(m, player_side))
    {
	return m;
    }
    else
    {
	return getMinimaxMove2D();
	//return getHeuristicMove(board, player_side);
    }
    //return getHeuristicMove(board, player_side);
    //getRandomMove();
}

/**
 * @brief gets the best move according to the minimax algorithm
 *        for arbitrary tree depth levels
 *
 * Currently does not implement alpha beta pruning
 */
RecursiveNode *Player::getMinimax(int levels, Side s, RecursiveNode *lastMove)
{
    //fprintf(stderr, "%d\n", levels);
    // Base case: just return the move itself
    if (levels <= 0)
    {
	// these are the only nodes we have to calculate the score for
	lastMove->setScore(score(lastMove->board));
	return lastMove;
    }
    // Recursive case:
    if (s == player_side) // minimizing node
    {
	// Create the child nodes
	std::vector<Move *> nextMoves = getMoves(lastMove->board,
						 opponent_side);
	std::vector<RecursiveNode *> nodes;
	RecursiveNode *worst = NULL;
	unsigned int worstIndex = 0;
	// Account for possibility that next moves is empty
	if (nextMoves.size() == 0)
	{
	    // Just score the current board and return
	    lastMove->setScore(score(lastMove->board));
	    return lastMove;
	}
	else
	{
	    // We don't care about the return value here, just the
	    // side effect of getting nextMoves[0]->score
	    Board *b = lastMove->board->copy();
	    b->doMove(nextMoves[0], opponent_side);
	    RecursiveNode *r = new RecursiveNode(nextMoves[0], b);
	    nodes.push_back(r);
	    getMinimax(levels - 1, opponent_side, nodes[0]);
	    int score = nodes[0]->getScore();
	    worst = nodes[0];
	    // Look through all the moves and pick the max score
	    for (unsigned int i = 1; i < nextMoves.size(); i++)
	    {
		b = lastMove->board->copy();
		b->doMove(nextMoves[i], opponent_side);
		nodes.push_back(new RecursiveNode(nextMoves[i], b));
		getMinimax(levels - 1, opponent_side, nodes[i]);
		int thisScore = nodes[i]->getScore();
		if (thisScore < score)
		{
		    score = thisScore;
		    worst = nodes[i];
		    worstIndex = i;
		}
	    }
	    // Now that we have the best, set the current node's score to that
	    lastMove->setScore(score);
	}
	// Free all the memory
	for (unsigned int i = 0; i < nextMoves.size(); i++)
	{
	    if (i != worstIndex)
	    {
		//delete nextMoves[i];
		//delete nodes[i]->board;
		//delete nodes[i];
	    }
	}
	return worst;
    }
    else                  // maximizing node
    {
	// Create the child nodes
	std::vector<Move *> nextMoves = getMoves(lastMove->board,
						 player_side);
	std::vector<RecursiveNode *> nodes;
	RecursiveNode *best = NULL;
	unsigned int bestIndex = 0;
	// Account for possibility that next moves is empty
	if (nextMoves.size() == 0)
	{
	    // Just score the current board and return
	    lastMove->setScore(score(lastMove->board));
	    return lastMove;
	}
	else
	{
	    // We don't care about the return value here, just the
	    // side effect of getting nextMoves[0]->score
	    Board *b = lastMove->board->copy();
	    b->doMove(nextMoves[0], player_side);
	    RecursiveNode *r = new RecursiveNode(nextMoves[0], b);
	    nodes.push_back(r);
	    getMinimax(levels - 1, player_side, nodes[0]);
	    int score = nodes[0]->getScore();
	    best = nodes[0];
	    // Look through all the moves and pick the max score
	    for (unsigned int i = 1; i < nextMoves.size(); i++)
	    {
		b = lastMove->board->copy();
		b->doMove(nextMoves[i], player_side);
	       	nodes.push_back(new RecursiveNode(nextMoves[i], b));
		getMinimax(levels - 1, player_side, nodes[i]);
		int thisScore = nodes[i]->getScore();
		if (thisScore > score)
		{
		    score = thisScore;
		    best = nodes[i];
		    bestIndex = i;
		}
	    }
	    // Now that we have the best, set the current node's score to that
	    lastMove->setScore(score);
	}
	// Free all the memory
	for (unsigned int i = 0; i < nextMoves.size(); i++)
	{
	    if (i != bestIndex)
	    {
		//delete nextMoves[i];
		//delete nodes[i]->board;
		//delete nodes[i];
	    }
	}
	return best;
    }
    //return NULL; // such a good recursive case
}

Move *Player::getMinimaxMove2D()
{
    // CREATE THE MONSTER
    MinimaxNode *lastMove = new MinimaxNode(NULL, board->copy());
    std::vector<Move *> nextMoves = getMoves(lastMove->board, player_side);
    for (unsigned int i = 0; i < nextMoves.size(); i++){ // complete level 1 of the tree
	MinimaxNode *myMove = new MinimaxNode(nextMoves[i], lastMove->board->copy());
	myMove->board->doMove(nextMoves[i], player_side);
	std::vector<Move *> theirNextMoves = getMoves(myMove->board, opponent_side);
	for (unsigned int j = 0; j < theirNextMoves.size(); j++){ // complete level 2 of the tree
		MinimaxNode *theirMove = new MinimaxNode(theirNextMoves[j], myMove->board->copy());
		theirMove->board->doMove(theirNextMoves[j], opponent_side);
		theirMove->setScore(score(theirMove->board));
		myMove->addChild(theirMove);
	}
	lastMove->addChild(myMove);
    }
    
    // SCORE THE 1ST LEVEL
    for (unsigned int i = 0; i < lastMove->children.size(); i++){
	lastMove->children[i]->setScore(1000); // starts off good, will become worse
	for (unsigned int j = 0; j < lastMove->children[i]->children.size(); j++){
	    // sets the score in the 1st level to the worst score
	    // in the 2nd level
	    lastMove->children[i]->setScore(
		std::min(lastMove->children[i]->children[j]->getScore(),
		lastMove->children[i]->getScore()));
	}
    }
    
    // FIND THE BEST MOVE (Max of the 1st level)
    Move * bestMove = NULL;
    lastMove->setScore(-1000);
    for (unsigned int i = 0; i < lastMove->children.size(); i++){
	if (lastMove->children[i]->getScore() > lastMove->getScore()){
		bestMove = lastMove->children[i]->move;
		lastMove->setScore(lastMove->children[i]->getScore());
	}
    }
    
    // DELETE ALL THE THINGS
    // i think this does it right but not sure
    // nobody's ever sure until valgrind
    for (unsigned int i = 0; i < lastMove->children.size(); i++){
	for (unsigned int j = 0;
		j < lastMove->children[i]->children.size(); j++){
		delete lastMove->children[i]->children[j];
	}
	delete lastMove->children[i];
    }
    delete lastMove;
    
    return bestMove;
}

Move *Player::getHeuristicMove(Board * board, Side side)
{
    // Remember to free all the moves in the list
    std::vector<Move *> moves = getMoves(board, side);

    Move *best = NULL;
    int maxScore = -100;
    for (unsigned int i = 0; i < moves.size(); i++)
    {
	Board *buffer = board->copy();
	buffer->doMove(moves[i], player_side);
	if (best == NULL)
	{
	    maxScore = score(buffer);
	    best = moves[i];
	}
	else
	{
	    int newScore = score(buffer);
	    if (newScore > maxScore)
	    {
		maxScore = newScore;
		best = moves[i];
	    }
	}
	// Don't leak memory
	delete buffer;
    }
    return best;
}

Move *Player::getRandomMove()
{
    // Remember to free all the moves in the list
    std::vector<Move *> moves = getMoves(board, player_side);

    // Check for no available moves
    if (moves.size() == 0) {
		return NULL;
    }
    
    Move *random = moves[rand()%moves.size()];
    Move *ret = new Move(random->getX(), random->getY());
    
    // Free every move in moves
    for (unsigned int i = 0; i < moves.size(); i++) {
		delete moves[i];
    }
    
    return ret;
}

int Player::score(Board *board)
{
	if (phase == OPENING){
	    //fprintf(stderr, "opening\n");
	    //return (MIDMOBILITYWEIGHT * mobility(board)) + special_move_score(board); // can't tell if this helped
	    return simple_score(board);
	} else if (phase == MIDGAME){
	    //fprintf(stderr, "midgame\n");
	    //return better_score(board) + (MIDMOBILITYWEIGHT * mobility(board));
	    return simple_score(board);
	} else if (phase == LATEGAME){
	    //fprintf(stderr, "lategame\n");
	    //return better_score(board);
	    return simple_score(board);
	}
	// assuming endgame or problems
	//fprintf(stderr, "endgame\n");
	return real_score(board);
}

int Player::mobility(Board *board){
	return getMoves(board, player_side).size() - getMoves(board, opponent_side).size();
}

int Player::real_score(Board *board){
	if (player_side == WHITE){
		return board->countWhite() - board->countBlack();
    } else {
		return board->countBlack() - board->countWhite();
    }
}

int Player::special_move_score(Board *board){
	int score = 0;
	
	// multiplier for corners
    if (board->get(player_side, 7, 7)) score += SELF_CORNER;
    else if (board->get(opponent_side, 7, 7)) score -= OPPONENT_CORNER;
    if (board->get(player_side, 0, 7)) score += SELF_CORNER;
    else if (board->get(opponent_side, 0, 7)) score -= OPPONENT_CORNER;
    if (board->get(player_side, 0, 0)) score += SELF_CORNER;
    else if (board->get(opponent_side, 0, 0)) score -= OPPONENT_CORNER;
    if (board->get(player_side, 7, 0)) score += SELF_CORNER;
    else if (board->get(opponent_side, 7, 0)) score -= OPPONENT_CORNER;
    
    // multiplier for next to corners
    for (unsigned int i = 0; i < nextToCorners.size(); i++) {
		if (board->get(player_side, nextToCorners[i].getX(), nextToCorners[i].getY())) score -= SELF_NEXTTOCORNER;
		if (board->get(opponent_side, nextToCorners[i].getX(), nextToCorners[i].getY())) score += OPPONENT_NEXTTOCORNER;
    }

    // multiplier for edges
    for (unsigned int i = 0; i < edges.size(); i++) {
		if (board->get(player_side, nextToCorners[i].getX(), nextToCorners[i].getY())) score += SELF_EDGE;
		if (board->get(opponent_side, nextToCorners[i].getX(), nextToCorners[i].getY())) score -= OPPONENT_EDGE;
    }
    
    return score;
}

int Player::better_score(Board *board){
	int score;
    if (player_side == WHITE){
		score = board->countWhite() - board->countBlack();
    } else {
		score = board->countBlack() - board->countWhite();
    }
    
    score += special_move_score(board);
    
    return score;
}

/**
 * @brief
 * moves is currently length 1
 */
int Player::simple_score(Board *board)
{
    int score;
    if (player_side == WHITE){
	score = board->countWhite() - board->countBlack();
    } else {
	score = board->countBlack() - board->countWhite();
    }

    // Multiplier for corners and areas near corners
    // TODO: make this more organized
    if (board->get(player_side, 7, 7))
    {
	score += 3;
    }
    else if (board->get(opponent_side, 7, 7))
    {
	score -= 3;
    }
    if (board->get(player_side, 0, 7))
    {
	score += 3;
    }
    else if (board->get(opponent_side, 0, 7))
    {
	score -= 3;
    }
    if (board->get(player_side, 0, 0))
    {
	score += 3;
    }
    else if (board->get(opponent_side, 0, 0))
    {
	score -= 3;
    }
    if (board->get(player_side, 7, 0))
    {
	score += 3;
    }
    else if (board->get(opponent_side, 7, 0))
    {
	score -= 3;
    }
    
    for (unsigned int i = 0; i < nextToCorners.size(); i++)
    {
	if (board->get(player_side, nextToCorners[i].getX(),
			   nextToCorners[i].getY()))
	{
	    score -= 2;
	}
	if (board->get(opponent_side, nextToCorners[i].getX(),
			   nextToCorners[i].getY()))
	{
	    score += 2;
	}
    }

    // Multiplier for edges
    for (unsigned int i = 0; i < edges.size(); i++)
    {
	if (board->get(player_side, nextToCorners[i].getX(),
			   nextToCorners[i].getY()))
	{
	    score += 2;
	}
	if (board->get(opponent_side, nextToCorners[i].getX(),
			   nextToCorners[i].getY()))
	{
	    score -= 2;
	}
    }
    
    return score;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    // Firstly, make the opponent's last move on our board
    board->doMove(opponentsMove, opponent_side);
    
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */
    /**
     * HEURISTIC WILL CONTAIN:
     * 		Corners = VERY VERY GOOD
     * 		just out from corners = VERY VERY BAD if the opponent can 
     * 			get a corner, BAD otherwise
     * 		2 out from corners = FAIRLY GOOD
     * 		Fewer # and quality of opponent moves = PRETTY GOOD
     * 		Convexity: try to make a convex shape from the corner
     * 		
     * 		Beginning game: use a playbook, and try to stay surrounded
     * 		Midgame: Convexity, and quality of opponent moves
     * 		Endgame: Calculate Minimax and play from there. Does
     * 			maximizing the amount you win by matter?
     **/

    // TODO: Use a better heuristic that this
    // "the random heuristic"
    Move *m = getMove(opponentsMove);

    // do the move on player's board
    board->doMove(m, player_side);
    
    numTurns++;
    if (numTurns > ENDTHRESHOLD){
	phase = ENDGAME;
    }
    else if (numTurns > LATETHRESHOLD){
	phase = LATEGAME;
    } else if (numTurns > MIDTHRESHOLD){
	phase = MIDGAME;
    }
    return m;
}

void Player::setBoard(Board *newBoard)
{
    // free the old board
    delete board;
    // set board to the new board
    board = newBoard;
}
