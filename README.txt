Contributions from each partner:

First assignment:
Carly:
Outlined scoring heuristic and eventual algorithm hopes and dreams
Made a scoring heuristic in simple_score
Made 2D minimax function

Nick:
Implemented random move heuristic
Used simple heuristic to choose the best move
Improved simple heuristic to better scoring by adding weights for corners, edges, and next to corners
Fixed Carly's 2D minimax segfaults

Second assignment:
Carly:
Created the MinimaxNode class
Added better_score with defined variables for easy changing
Added scoring based on mobility
Added game phases (see below)

Nick:
Cleanup/structural changes
Created RecursiveNode structure for more than 2D minimax
Optimized the Board class (not currently in effect)


Improvements:
Aside from implementing 2D minimax and better scoring asked for in the assignment,
we implemented the following:
MOBILITY: simply the number of your available moves - the number of their available moves.
Very useful for midgame.
GAME PHASES: We split the game into five phases, based on the number of moves we've taken. 
BOOK is the first phase, where we pick our moves from an opening book (not implemented). 
OPENING chooses moves based on mobility alone. 
MIDGAME chooses moves based on mobility and better scoring. 
LATEGAME chooses moves based on better scoring. All three middle phases use 2D minimax.
ENDGAME chooses moves based on exact number of pieces.
MINIMAX TO MORE DEPTH: in the endgame, run minimax to more depth so you don't make
costly mistakes at the end

We think our strategy, particularly the game phasing, will be effective, since different strategies
work and fail at different stages of the game. For example, our better heuristic assigns extra
points to corners and edges, which is good in the midgame since it encourages choices that
will not be detrimental later on. But later in the game, this leads to incorrect calculations of
how many pieces each player has. Mobility is useful early on, but eventually you have to start
capturing the pieces. So a game phasing that respects the usefulness of each strategy allows 
us to be competitive at every stage of the game.

Not implemented:
Optimization of Board class to only search for available moves around the current pieces.
"Concavity": the idea that around the corners, you can build a concave shape that nobody can flip
any of your pieces inside. These pieces are now YOURS FOREVER so we should be establishing
concave hulls.
